//
//  ViewController.swift
//  Rock Paper Scissors
//
//  Created by Brandon Rodriguez on 10/17/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, MCBrowserViewControllerDelegate, MCSessionDelegate {
    
    @IBOutlet weak var playOutlet: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var peerID: MCPeerID!
    var session: MCSession!
    var browser: MCBrowserViewController!
    var advertiser: MCAdvertiserAssistant!
    
    let concurrentQueue = dispatch_queue_create("co.brandonr.vc", DISPATCH_QUEUE_CONCURRENT)
    let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light));
    let serviceID = "Brandon-RPS";
    var connacted: Bool = false;
    var ready: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let backgroundImage: UIImage? = UIImage(named: "background.jpg");
        if let image = backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
            
            // Setting stuffs
            blurView.frame = self.view.bounds;
            self.view.addSubview(blurView);
        }
        
        playOutlet.layer.cornerRadius = 8;

        for view in [playOutlet, titleLabel] {
            view.layer.masksToBounds = false;
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 5;
            view.layer.shadowOpacity = 0.25;
            self.view.addSubview(view);
        }
        
        // Multipeer Stuffs
        peerID = MCPeerID(displayName: UIDevice.currentDevice().name);
        
        session = MCSession(peer: peerID);
        
        advertiser = MCAdvertiserAssistant(serviceType: serviceID, discoveryInfo: nil, session: session);
        advertiser.start();
        
    }

    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true);
        session.delegate = self;
        ready = false;
    }

    @IBAction func playAction(sender: AnyObject) {
        // If we're "connacted" to our opponent we check if the opponent is ready, then send the go signal to start the match. If not we tell the opponent that we're ready.
        if connacted {
            if ready {
                do {
                    try session.sendData("go".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
                self.performSegueWithIdentifier("toSelection", sender: self);
            } else {
                do {
                    try session.sendData("ready".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
            }
        } else {
            browser = MCBrowserViewController(serviceType: serviceID, session: session);
            browser.maximumNumberOfPeers = 1;
            browser.delegate = self;
            self.presentViewController(browser, animated: true, completion: {});
        }

    }
    
    // MARK: - MCBrowserViewControllerDelegate required functions
    
    // Notifies the delegate, when the user taps the done button.
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
        browserViewController.dismissViewControllerAnimated(true, completion: nil);
    }
    
    // Notifies delegate that the user taps the cancel button.
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
        browserViewController.dismissViewControllerAnimated(true, completion: nil);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toSelection" {
            // Need our session in the next controller.
            let gameController: GameController = (segue.destinationViewController as? GameController)!;
            gameController.session = session;
        }
    }
    
    // MARK: - MCSessionDelegate required functions
    
    // Remote peer changed state.
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        dispatch_async(dispatch_get_main_queue(), {
            
            // Once we're connected to our opponent, close the multipeer browser because we no longer need it. We're only battling one person.
            switch state {
                case MCSessionState.Connected:
                    if session.connectedPeers.count > 1 {
                        
                    } else {
                        
                        self.connacted = true;
                        if let controller = self.browser {
                            controller.dismissViewControllerAnimated(true, completion: nil);
                        }

                        dispatch_async(self.concurrentQueue, {
                            
                        })
                        
                        self.advertiser.stop();
                        
                    }
                    break;
                case MCSessionState.Connecting:
                    break;
                case MCSessionState.NotConnected:
                    self.connacted = false;
                    break;
            }
        })
    }
    
    // Received data from remote peer.
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        
        // If peer is ready we set our flag to true so that we know we can continue.
        if data.isEqualToData("ready".dataUsingEncoding(NSUTF8StringEncoding)!) {
            ready = true;
        } else if data.isEqualToData("go".dataUsingEncoding(NSUTF8StringEncoding)!) {
            // This is the signal that is sent once both players have pressed play after connecting.
            dispatch_async(dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("toSelection", sender: self);
            })
        }
        
    }
    
    // Received a byte stream from remote peer.
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    // Start receiving a resource from remote peer.
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        
    }
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        
    }
    
    

}

