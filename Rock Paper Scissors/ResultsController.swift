//
//  ResultsController.swift
//  Rock Paper Scissors
//
//  Created by Brandon Rodriguez on 10/20/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class ResultsController: UIViewController {
    
    var selected: String!
    var peerSelected: String!
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;

    @IBOutlet weak var youChose: UILabel!
    @IBOutlet weak var youLabel: UILabel!
    @IBOutlet weak var opponentChose: UILabel!
    @IBOutlet weak var opponentLabel: UILabel!
    @IBOutlet weak var replayBtn: UIButton!
    @IBOutlet weak var winsStatic: UILabel!
    @IBOutlet weak var lossStatic: UILabel!
    @IBOutlet weak var wins: UILabel!
    @IBOutlet weak var losses: UILabel!
    
    var resultAlert: UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let backgroundImage: UIImage? = UIImage(named: "background.jpg");
        if let image = backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
            
            
            // Cool blurry effect
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light));
            
            // Setting stuffs
            blurView.frame = self.view.bounds;
            self.view.addSubview(blurView);
        }
        for view in [youChose, opponentChose, youLabel, opponentLabel, replayBtn, wins, losses, winsStatic, lossStatic] {
            view.layer.masksToBounds = false;
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 5;
            view.layer.shadowOpacity = 0.25;
            self.view.addSubview(view)
        };
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true);
        
        // Double checking our choices.
        if let iChose = selected {
            youChose.text = iChose;
        } else {
            youChose.text = "What did you pick?!";
        }
        
        if let peerChose = peerSelected {
            opponentChose.text = peerChose;
        } else {
            opponentChose.text = "What the heck did the peer pick?!";
        }

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true);
        // Once the view has appeared we can enable the replay button. Just in case.
        replayBtn.userInteractionEnabled = true;
        incrementScore(playerWon());
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    func playerWon() -> Bool {
        let iChose: String = youChose.text!;
        let rock: String = "Rock";
        let paper: String = "Paper";
        let scissors: String = "Scissors";
        let nothing: String = "Nothing";
        
        var victory: Bool = false;
        
        if let opponentChose = opponentChose.text {
            switch opponentChose {
            case nothing:
                if iChose == nothing {
                    // If anything but nothing, return true
                } else {
                    victory = true;
                }
            case rock:
                if iChose == paper {
                    victory = true;
                }
            case paper:
                if iChose == scissors {
                    victory = true;
                }
            case scissors:
                if iChose == rock {
                    victory = true;
                }
            default:
                victory = false;
            }
        }
        
        return victory;
    }
    
    func incrementScore(result: Bool) {
        
        // Both chose nothing, or is it a draw? Too bad you lose anyway.
        // Decided to throw in win/lose notification in here as well. Didn't want to make function name too long.
        if result == true {
            appDelegate.iWon = ++appDelegate.iWon
            resultAlert = UIAlertController(title: "Congratulations!", message: "You've won!", preferredStyle: UIAlertControllerStyle.Alert)
            resultAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler: {action in
                self.resultAlert.dismissViewControllerAnimated(true, completion: nil);
            }))
            self.presentViewController(resultAlert, animated: true, completion: nil);
        } else {
            resultAlert = UIAlertController(title: "Aww :(", message: "You lost...", preferredStyle: UIAlertControllerStyle.Alert)
            resultAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler: {action in
                self.resultAlert.dismissViewControllerAnimated(true, completion: nil);
            }))
            self.presentViewController(resultAlert, animated: true, completion: nil);
            appDelegate.opponentWon = ++appDelegate.opponentWon
        }
        wins.text = appDelegate.iWon.description;
        losses.text = appDelegate.opponentWon.description;

    }
    
    @IBAction func replayTapped(sender: UIButton) {
        // Back to the start!
        self.navigationController?.popToRootViewControllerAnimated(true);
    }


}
