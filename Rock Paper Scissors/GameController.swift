//
//  GameController.swift
//  Rock Paper Scissors
//
//  Created by Brandon Rodriguez on 10/17/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class GameController: UIViewController, MCSessionDelegate {
    
    @IBOutlet weak var rockBtn: UIButton!
    @IBOutlet weak var scissorsBtn: UIButton!
    @IBOutlet weak var paperBtn: UIButton!
    @IBOutlet weak var countdownLabel: UILabel!
    
    var timer: NSTimer = NSTimer();
    var count: Int = 3;
    var session: MCSession!
    var selected: String!
    var peerSelected: String!
    
    let concurrentQueue = dispatch_queue_create("co.brandonr.gc", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let backgroundImage: UIImage? = UIImage(named: "background.jpg");
        if let image = backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
            
            
            // Cool blurry effect
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light));
            
            // Setting stuffs
            blurView.frame = self.view.bounds;
            self.view.addSubview(blurView);
        }
        for view in [rockBtn, paperBtn, scissorsBtn, countdownLabel] {
            view.layer.masksToBounds = false;
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 5;
            view.layer.shadowOpacity = 0.25;
            self.view.addSubview(view)
        };        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true);
        // The session's delegate methods need to run from this controller for now
        session.delegate = self;
        // Once the view appears, we can begin the battle and enable the buttons then start the timer.
        for button in [rockBtn, paperBtn, scissorsBtn] {
            button.userInteractionEnabled = true;
        };
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: Selector("decrementCount"), userInfo: nil, repeats: true);
    }
    
    // Was easier to make this function then write it out everywhere.
    func disableButtons() {
        for button in [rockBtn, paperBtn, scissorsBtn] {
            button.userInteractionEnabled = false;
        };
    }
    
    func decrementCount () {
        countdownLabel.text = String(--count);
        
        // Once we've reached 0, stop the timer, disable the buttons, and perform our segue to the results.
        if count == 0 {
            timer.invalidate();
            dispatch_async(dispatch_get_main_queue(), {
                self.disableButtons();
                self.performSegueWithIdentifier("toResults", sender: self)
            });
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // If we're going to the results, set our result view controllers properties.
        if segue.identifier == "toResults" {
            // If I, or the opponent did not choose anything, the variables will be nil, therefore we tell the next controller that we selected nothing.
            let resultsController: ResultsController = (segue.destinationViewController as? ResultsController)!;
            if let iChose = selected {
                resultsController.selected = iChose;
            } else {
                resultsController.selected = "Nothing";
            }
            
            if let peerChose = peerSelected {
                resultsController.peerSelected = peerChose;
            } else {
                resultsController.peerSelected = "Nothing";
            }
        }
    }
    
    @IBAction func btnPressed(sender: UIButton) {
        // Send our choice, set the variables, and disable those buttons!
        switch sender.tag {
            case 0:
                do {
                    try session.sendData("rock".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
                selected = "Rock";
                disableButtons();
                break;
            case 1:
                do {
                    try session.sendData("paper".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
                selected = "Paper";
                disableButtons();
                break;
            case 2:
                do {
                    try session.sendData("scissors".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
                selected = "Scissors";
                disableButtons();
                break;
            default:
                print("Something unknown tapped");
        }
    }
    
    // MARK: - MCSessionDelegate required functions
    
    // Remote peer changed state.
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        // If we get disconnected at any point, it's probably best we just go back to the first view controller and try this again.
        dispatch_async(dispatch_get_main_queue(), {
            
            switch state {
            case MCSessionState.Connected:
                break;
            case MCSessionState.Connecting:
                break;
            case MCSessionState.NotConnected:
                self.navigationController?.popToRootViewControllerAnimated(true);
                break;
            }
        })
    }
    
    // Received data from remote peer.
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        // Based on what received from our peer, set the variable to the corresponding choice.
        switch data {
            case "rock".dataUsingEncoding(NSUTF8StringEncoding)!:
                peerSelected = "Rock";
                break;
            case "paper".dataUsingEncoding(NSUTF8StringEncoding)!:
                peerSelected = "Paper";
                break;
            case "scissors".dataUsingEncoding(NSUTF8StringEncoding)!:
                peerSelected = "Scissors";
                break;
            default:
                peerSelected = nil;
            
        }
        
    }
    
    // Received a byte stream from remote peer.
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    // Start receiving a resource from remote peer.
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        
    }
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        
    }

}
